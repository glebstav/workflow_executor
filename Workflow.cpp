#include "Workflow.h"

using namespace Stavnichy;
	Workflow::Workflow() : text(*new map<int, string>){
	};
	Workflow::~Workflow() {
		delete &text;
	}
	void Workflow::start(const char *instructions_file) {
		_instructions_file.open(instructions_file);
		if (!_instructions_file.is_open()) {
			cout << "Can not open file : " << instructions_file << endl;
			return;
		}
		getInstructions();
	}
	void Workflow::getInstructions() {
			getline(_instructions_file, _line_of_instructions);
		while ((!_instructions_file.eof()) && (_line_of_instructions.find("desc") == -1)) {
			getline(_instructions_file, _line_of_instructions);
		}
			getline(_instructions_file, _line_of_instructions);
		while ((!_instructions_file.eof()) && _line_of_instructions.find("csed",0) == -1) {
			instruction.add(_line_of_instructions);
			blocks[atoi(_line_of_instructions.c_str())] = getBlock(atoi(_line_of_instructions.c_str()));
			getline(_instructions_file, _line_of_instructions);
		}
		getQueue();
	};

	void Workflow::getQueue() {
		getline(_instructions_file, _line_of_instructions);
		int i = 0;
		while(i !=-1 ){
			_list.push(atoi(_line_of_instructions.c_str()));
			
			i = _line_of_instructions.find("->");
			_line_of_instructions = _line_of_instructions.substr(i+2, _line_of_instructions.length() - i - 2);
		}
		doList();
	}
	void Workflow::doList() {
		int command;
		int cnt_of_executed_commands = 0;
		while(!_list.empty()){
			
			blocks[_list.front()]->execute(text);

			_list.pop();
			cnt_of_executed_commands++;
		}
		cout << "Last command must be 'writefile'";
		return;
	}


	Executor* Workflow::getBlock(int num_of_command) {
		Executor *Block;
		int command = instruction.get(num_of_command);
		switch (command) {
		case  readfile: {
			string filename = instruction.getFirstArg(num_of_command);
			Block = new ReadFile(filename);
			return Block;
		}
		case writefile: {
			string filename = instruction.getFirstArg(num_of_command);
			Block = new WriteFile(filename);
			return Block;
		}
		case grep: {
			string word = instruction.getFirstArg(num_of_command);
			Block = new Grep(word);
			return Block;
		}
		case sort: {
			Block = new Sort();
			return Block;
		}
		case replace: {
			string word1 = instruction.getFirstArg(num_of_command);
			string word2 = instruction.getSecondArg(num_of_command);
			Block = new Replace(word1, word2);
			return Block;
		}
		case dump: {
			string filename = instruction.getFirstArg(num_of_command);
			Block = new Dump(filename);
			return Block;
		}
		}

		
	}



