#include "replace.h"
namespace Stavnichy {
	Replace::Replace(string Word1,string Word2): word1(Word1), word2(Word2){

	}
	void Replace::execute(map<int, string> &text) {
		int position;

		for (auto line = text.begin(); line != text.end(); line++)
		{
			position = 0;

			while (position < text[line->first].length() && text[line->first].find(word1, position) != -1) {
				position = text[line->first].find(word1, position);
				text[line->first].erase(position, word1.length());
				text[line->first].insert(position, word2);
				position += word2.length();

			}
		}
	};
}