#pragma once
#include "Executor.h";

#include <map>;
#include <string>;
#include <iostream>;

using std::map;
using std::string;
using std::cout;
using std::endl;
namespace Stavnichy {
	class Replace: public Executor{
	public:
		Replace(string Word1, string Word2);
		void execute(map<int, string> &text) override;
	private:
		string word1;
		string word2;
	};
}