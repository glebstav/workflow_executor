#include "Workflow.h"

Stavnichy::Workflow::InstructionS::InstructionS() {
	
}
void Stavnichy::Workflow::InstructionS::add(string _line_of_instructions) {
	int num = atoi(_line_of_instructions.c_str());

	_list_of_instr[num] = _line_of_instructions.substr(_line_of_instructions.find('=') + 1);
	while (_list_of_instr[num][0]==' ') {
		_list_of_instr[num].erase(0,1);
	}
	
}

int Stavnichy::Workflow::InstructionS::get(int num_of_instruction)  {
	string instruction = _list_of_instr[num_of_instruction].substr(0, _list_of_instr[num_of_instruction].find(' '));
	if (instruction == "readfile") {
		return readfile;
	}
	if (instruction == "writefile") {
		return writefile;
	}
	if (instruction == "grep") {
		return grep;
	}
	if (instruction == "sort") {
		return sort;
	}
	if (instruction == "replace") {
		return replace;
	}
	if (instruction == "dump") {
		return dump;
	}
	return wrongcommand;
}

string Stavnichy::Workflow::InstructionS::getFirstArg(int num_of_instruction)  {
	string firstArg = _list_of_instr[num_of_instruction].substr(_list_of_instr[num_of_instruction].find(' '));
	while (firstArg[0] == ' ') {
		firstArg.erase(0,1);
	}
	firstArg = firstArg.substr(0, firstArg.find(' ') == -1 ? firstArg.length() : firstArg.find(' '));
	return firstArg;
};


string Stavnichy::Workflow::InstructionS::getSecondArg(int num_of_instruction) {
	string secondArg = _list_of_instr[num_of_instruction].substr(_list_of_instr[num_of_instruction].find(' '));
	while (secondArg[0] == ' ') {
		secondArg.erase(0,1);
	}
	secondArg = secondArg.substr(secondArg.find(' '));
	while (secondArg[0] == ' ') {
		secondArg.erase(0,1);
	}
	secondArg = secondArg.substr(0, secondArg.find(' ') == -1 ? secondArg.length() : secondArg.find(' '));
	return secondArg;
};
