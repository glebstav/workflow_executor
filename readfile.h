#pragma once
#include "Executor.h";
#include <string>;
#include <fstream>;
#include <map>;
using std::string;
using std::ifstream;
using std::getline;
using std::map;
namespace Stavnichy{
	class ReadFile: public Executor{
	public:
		ReadFile(string filename);
		void execute(map<int, string> &text) override;
	private:
		ifstream File;
	};
}
