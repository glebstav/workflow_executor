#include "grep.h"

namespace Stavnichy {
	Grep::Grep(string Word) : word(Word) {
	
	};

	void Grep::execute(map<int, string> &text) {
		for (auto line = text.begin(); line != text.end(); )
		{
			if (line->second.find(word) == -1) {
				text.erase(line++->first);
			}
			else
				line++;
		}
	}
}