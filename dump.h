#pragma once
#include "Executor.h";
#include <fstream>;
#include <map>;
#include <string>;
using std::ofstream;
using std::string;
using std::map;
using std::endl;
namespace Stavnichy {
	class Dump: public Executor{
	public:
		Dump(string filename);
		void  execute(map<int, string> &text) override;
	private:
		ofstream File;
	};
}

