
#pragma once
#include "Executor.h";
#include "readfile.h";
#include "writefile.h";
#include "grep.h";
#include "sort.h";
#include "dump.h"
#include "replace.h";
#include <fstream>;
#include <string>;
#include <iostream>;
#include <queue>;

using std::ifstream;

using std::string;
using std::queue;
using std::getline;
using std::cout;
using std::endl;
namespace Stavnichy {
	enum commands {
		readfile = 1,
		writefile = 2,
		grep = 3,
		sort = 4,
		replace = 5,
		dump = 6,
		wrongcommand = 0
	};

	class Workflow
	{
	public:
		Workflow();
		~Workflow();
		void start(const char *instructions_file);

		

			class InstructionS {
			public:
			
				InstructionS();
				void add(string _line_of_instructions);
				int get(int num_of_instruction);
				string getFirstArg(int num_of_instruction);
				string getSecondArg(int num_of_instruction);
			private:
				map <int, string> _list_of_instr;
			};


	private:
		
		void getInstructions();
		void getQueue();
		void doList();
		Executor* getBlock(int num_of_command);
		InstructionS instruction;
		ifstream _instructions_file;
		string _line_of_instructions;
		
		map <int, string> &text;
		map <int, Executor*> blocks;
		queue <int> _list;
		int _cnt_of_command = 0;
		
	};

}