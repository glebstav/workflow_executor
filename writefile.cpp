#include "writefile.h"

namespace Stavnichy {
	WriteFile::WriteFile(string filename) {
		File.open(filename);
	}

	void WriteFile::execute(map<int, string> &text) {
		for (auto line = text.begin(); line != text.end(); line++)
		{
			File << line->second << endl;
		}
	};
}