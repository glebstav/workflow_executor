#pragma once

#include <map>;
#include <string>;
using std::string;
using std::map;

namespace Stavnichy {
	class Executor
	{
	public:
		virtual void execute(map<int, string> &text) = 0;
	};
}
