#pragma once
#include "Executor.h";
#include <fstream>;
#include <map>;
#include <string>;
using std::ofstream;
using std::string;
using std::map;
using std::endl;
namespace Stavnichy {
	class WriteFile: public Executor{
	public:
		WriteFile(string filename);
		void execute(map<int, string> &text) override;
	private:
		ofstream File;
	};
}
