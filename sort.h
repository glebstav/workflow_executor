#pragma once
#include "Executor.h";
#include <map>;
#include <string>;
#include <iostream>;
using std::map;
using std::string;
using std::cout;
using std::endl;
namespace Stavnichy {
	class Sort :public Executor {
	public:
		Sort();
		void execute(map<int, string> &text) override;
	};
}
