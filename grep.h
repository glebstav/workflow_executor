#pragma once
#include "Executor.h";

#include <map>;
#include <string>;
#include <iostream>;

using std::map;
using std::string;
using std::cout;
using std::endl;
namespace Stavnichy {
	class Grep :public Executor {
	public:
		Grep(string Word);
		void execute(map<int, string> &text) override;
	private:
		string word;
	};
}