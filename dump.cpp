#include "dump.h"

namespace Stavnichy {
	Dump::Dump(string filename){
		File.open(filename);
	};

	void Dump::execute(map<int, string> &text) {
		for (auto line = text.begin(); line != text.end(); line++)
		{
			File << line->second << endl;
		}
	};
}